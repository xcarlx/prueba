from django import forms

from apps.rrhh.models import Persona


class PersonaForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = "__all__"
        widgets = {
            "nacimiento": forms.DateInput(attrs={"type": "date"}, format="%Y-%m-%d")
        }

    def __init__(self, *args, **kwargs):
        super(PersonaForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = "form-control"

    def clean(self):
        dni = self.cleaned_data['dni']
        if dni == "12345678":
            self.add_error("dni", "Error")
            raise forms.ValidationError("no puedes colocar este dni")
        return super(PersonaForm, self).clean()

    # def save(self, commit=True):
    #     persona = super(PersonaForm, self).save(commit=False)
    #     persona.nombres = self.cleaned_data['nombres']
    #     persona.dni = self.cleaned_data['dni']
    #     persona.nacimiento = self.cleaned_data['nacimiento']
    #     # persona.save()
    #     return persona
