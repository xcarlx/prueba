from django import forms

from apps.rrhh.models import PeriodoTrabajo


class PeriodoTrabajoForm(forms.ModelForm):
    class Meta:
        model = PeriodoTrabajo
        fields = "__all__"
        widgets = {
            "inicio": forms.DateInput(attrs={"type": "date"}, format="%Y-%m-%d"),
            "fin": forms.DateInput(attrs={"type": "date"}, format="%Y-%m-%d")
        }

    def __init__(self, *args, **kwargs):
        super(PeriodoTrabajoForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = "form-control"
        self.fields['persona'].widget = forms.HiddenInput()

    # def save(self, commit=True):
    #     persona = super(PersonaForm, self).save(commit=False)
    #     persona.nombres = self.cleaned_data['nombres']
    #     persona.dni = self.cleaned_data['dni']
    #     persona.nacimiento = self.cleaned_data['nacimiento']
    #     # persona.save()
    #     return persona
