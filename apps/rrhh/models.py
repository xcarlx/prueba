from django.db import models


# Create your models here.


class Persona(models.Model):
    nombres = models.CharField(max_length=100)
    dni = models.CharField(max_length=8)
    nacimiento = models.DateField()

    def __str__(self):
        return f"{self.nombres}"


class PeriodoTrabajo(models.Model):
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT)
    inicio = models.DateField()
    fin = models.DateField(blank=True, null=True)
    cargo = models.CharField(max_length=150)
    area = models.CharField(max_length=150)

    def __str__(self):
        return f"{self.persona.nombres} - {self.cargo}"
