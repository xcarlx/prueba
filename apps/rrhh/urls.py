"""prueba URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from apps.rrhh.viewstotal import persona, periodo_trabajo

urlpatterns = [
    # PERSONA
    path('', persona.HomeView.as_view(), name='persona'),
    path('crear/', persona.CrearPersonaView.as_view(), name='persona-crear'),
    path('editar/<int:pk>/', persona.EditarPersonaView.as_view(), name='persona-editar'),
    path('eliminar/<int:pk>/', persona.EliminarPersonaView.as_view(), name='persona-eliminar'),

    # PERIODO TRABAJO
    path('periodo_trabajo/<int:pk>/', periodo_trabajo.HomeView.as_view(), name='periodo_trabajo'),
    path('periodo_trabajo/crear/<int:persona>/', periodo_trabajo.CrearPeriodoTrabajoView.as_view(), name='periodo_trabajo-crear'),
    path('periodo_trabajo/editar/<int:pk>/', periodo_trabajo.EditarPeriodoTrabajoView.as_view(),
         name='periodo_trabajo-editar'),
    path('periodo_trabajo/eliminar/<int:pk>/', periodo_trabajo.EliminarPeriodoTrabajoView.as_view(),
         name='periodo_trabajo-eliminar'),

]
