from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy, reverse
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView

from apps.rrhh.formstotal.persona import PersonaForm
from apps.rrhh.models import Persona


class HomeView(TemplateView):
    template_name = "rrhh/persona/home.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['persona'] = Persona.objects.all()
        return context


class CrearPersonaView(CreateView):
    model = Persona
    template_name = "rrhh/persona/form_persona.html"
    form_class = PersonaForm
    success_url = reverse_lazy('persona')


class EditarPersonaView(UpdateView):
    model = Persona
    template_name = "rrhh/persona/form_persona.html"
    form_class = PersonaForm
    success_url = reverse_lazy('persona')


class EliminarPersonaView(DeleteView):
    model = Persona
    template_name = "rrhh/persona/form_persona_eliminar.html"
    success_url = reverse_lazy('persona')
