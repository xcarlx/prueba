# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, DetailView

from apps.rrhh.formstotal.periodo_trabajo import PeriodoTrabajoForm
from apps.rrhh.models import Persona, PeriodoTrabajo


class HomeView(DetailView):
    model = Persona
    template_name = "rrhh/periodo_trabajo/home.html"

    # def get_context_data(self, **kwargs):
    #     context = super(HomeView, self).get_context_data(**kwargs)
    #     context['persona'] = Persona.objects.all()
    #     return context


class CrearPeriodoTrabajoView(CreateView):
    model = PeriodoTrabajo
    template_name = "rrhh/periodo_trabajo/formulario.html"
    form_class = PeriodoTrabajoForm
    success_url = reverse_lazy('persona')

    def get_initial(self):
        self.initial = {
            "persona": self.kwargs['persona']
        }
        return super(CrearPeriodoTrabajoView, self).get_initial();


class EditarPeriodoTrabajoView(UpdateView):
    model = PeriodoTrabajo
    template_name = "rrhh/periodo_trabajo/formulario.html"
    form_class = PeriodoTrabajoForm
    success_url = reverse_lazy('persona')


class EliminarPeriodoTrabajoView(DeleteView):
    model = PeriodoTrabajo
    template_name = "rrhh/periodo_trabajo/form_eliminar.html"
    success_url = reverse_lazy('persona')
